import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { Users } from './../interfaces/users';
import { UsersService } from './../users.service';
import { PostsService } from './../posts.service';
import { Posts } from './../interfaces/posts';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
//Post$:Posts[];
Post$:Observable<any>;
User$:Users[];

i:number;
j:number;
userId:string;

  constructor(private postsService:PostsService,private usersservice:UsersService, public authService:AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.Post$ = this.postsService.getPosts(this.userId);        }
    )

  }
  deletePost(id:string){
  
        this.postsService.deletePost(id,this.userId);      
    
   
  
  }
  /*onSubmit(){
    for(this.i=0;this.i<this.Post$.length;this.i++){
      for (this.j=0;this.j<this.User$.length;this.j++){
        if(this.Post$[this.i].userId==this.User$[this.j].id)
          this.postsservis.savePosts(this.Post$[this.i].title,this.Post$[this.i].body,this.User$[this.j].name) 
      }
    } 
  }*/




  }


