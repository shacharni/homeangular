import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  //authors:Object[]=[{id:1, name:'Lewis Carrol'},{id:2,name:'Leo Tolstoy'},{id:3,name:'Thomas Mann'}];

  constructor(private authorssrvice:AuthorsService, private route:ActivatedRoute) { }
  name;
  id;
  newName:string;
  authors:any;
  authors$:Observable<any>;
  ngOnInit() {
    this.name = this.route.snapshot.params.name;
    this.id = this.route.snapshot.params.id;
   this.authors$=this.authorssrvice.getAuthors();
   //this.authorssrvice.addAuthors();
  }
  onSubmit(){
    this.authorssrvice.addAuthors(this.newName);
  }
}

