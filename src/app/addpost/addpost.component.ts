import { PostsService } from './../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.css']
})
export class AddpostComponent implements OnInit {
  id:string;
  title:string;
  body:string;
  author:string;
  buttonText:string="Add post";
  isEdit:boolean=false;
  userId:string;

  constructor(public router:Router, public addpostService:PostsService, private route:ActivatedRoute,public authService:AuthService ) { }

  ngOnInit() 
  {
    this.id=this.route.snapshot.params.id;
   console.log(this.id);
   this.authService.user.subscribe
   (
          user => 
          {
            this.userId = user.uid;
            if(this.id){
              this.isEdit=true;
              this.buttonText="Update book";
              this.addpostService.getPost(this.id,this.userId).subscribe
              (
                post=> 
                {
                  console.log(post.data().author);
                  this.author=post.data().author;
                  this.title=post.data().title;
                  this.body=post.data().body;
                }
              )
             }
          
        }
  )
}

  onSubmit(){
    if(!this.isEdit){
     this.addpostService.addPosts(this.userId,this.title,this.body,this.author)
     }

else{
  this.addpostService.upDatePost(this.userId,this.id,this.title,this.body,this.author)
}
this.router.navigate(['/posts'])
}

   }


  