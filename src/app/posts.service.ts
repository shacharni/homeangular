

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Posts } from './interfaces/posts';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
 
  apiurl="https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient,private db:AngularFirestore) { }
  postCollection:AngularFirestoreCollection=this.db.collection('posts');
  userCollection:AngularFirestoreCollection=this.db.collection('users');
  
  //getPost(id:string):Observable<any>{ 
    
       // return this.db.doc(`posts/${id}`).get();
  //}
 
   getPosts(userId:string):Observable<any[]>{
    // return this.postCollection.valueChanges({idField:'id'}); 
     this.postCollection= this.db.collection(`users/${userId}/posts`);
     return this.postCollection.snapshotChanges().pipe(
       //pipe מאפשר לשרשר פונקציות
       map(
         //map גם מאפשר לשרשר פונקציות
         collection => collection.map(
           document => {
             const data = document.payload.doc.data();
             data.id= document.payload.doc.id;
             console.log(data);
             return data;
           }
         )
       )
     )
  }
 
    //return this.postCollection.valueChanges({idField:'id'});
   //return this.http.get<Posts[]>(this.apiurl);
   getPost(id:string,userId:string):Observable<any>{
      return this.db.doc(`users/${userId}/posts/${id}`).get();
     }
  savePosts(title_input:string, body_input:string,author_input:string)
  {
    const post={title:title_input,body:body_input,author:author_input};
    this.postCollection.add(post);

  }
 addPosts(userId:string,title_input:string, body_input:string,author_input:string)
  {
    const post={title:title_input,body:body_input,author:author_input};
   // this.postCollection.add(post);
   this.userCollection.doc(userId).collection('posts').add(post);

  }
  
  upDatePost(userId:string,id:string,title_input:string,body_input:string, author_input:string)
{
         this.db.doc(`users/${userId}/posts/${id}`).update({title:title_input,body:body_input, author:author_input});}
 
  deletePost(id:string,userId:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
    
  }
 
}