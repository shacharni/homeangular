// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseconfig: {
    apiKey: 'AIzaSyABvm9KNET264lJAgm8t-rBd_WksC_-QT0',
    authDomain: 'angularhome.firebaseapp.com',
    databaseURL: 'https://angularhome.firebaseio.com',
    projectId: 'angularhome',
    storageBucket: 'angularhome.appspot.com',
    messagingSenderId: '397757424715',
    appId: '1:397757424715:web:2ab7cf90cc53b925e599ce',
    measurementId: 'G-F9QVC8YV9Z'
  }
  };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
